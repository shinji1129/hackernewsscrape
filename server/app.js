const express = require('express');
const { graphqlHTTP } = require('express-graphql')
const schema = require('./schema/schema')
const app = express();
const path = require("path")
const fetch = require('./schema/fetch')
const cors = require('cors')

app.use(express.static(path.join(__dirname, "../build")))

const main = () => {
    setInterval(async () => {
        await fetch.fetchText()
        console.log('Database updated')
    }, 59000)
}
main()

app.use(cors())

app.use('/graphql', graphqlHTTP({
    schema,
    graphiql: true
}))

app.listen(4000, () => {
    console.log('Listening on port 4000')
})