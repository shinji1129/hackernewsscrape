const fs = require('fs')
const graphql = require("graphql")
const _ = require('lodash')
const path = require("path");

const {
    GraphQLObjectType,
    GraphQLString,
    GraphQLInt,
    GraphQLID,
    GraphQLList,
    GraphQLSchema
} = graphql

const posts = JSON.parse(fs.readFileSync(path.join(__dirname + '/database.json')))

const PostType = new GraphQLObjectType({
    name: 'Post',
    fields: () => ({
        id: { type: GraphQLInt },
        title: { type: GraphQLString },
        author: { type: GraphQLString },
        commentCount: { type: GraphQLString },
        points: { type: GraphQLString },
        postTime: { type: GraphQLString },
        link: { type: GraphQLString }
    })
})

const RootQuery = new GraphQLObjectType({
    name: 'RootQueryType',
    fields: {
        post: {
            type: PostType,
            args: { id: { type: GraphQLInt } },
            resolve(parent, args) {
                return _.find(posts, { id: args.id })
            }
        },
        posts: {
            type: GraphQLList(PostType),
            resolve() {
                return posts
            }
        },
    }
})

module.exports = new graphql.GraphQLSchema({
    query: RootQuery,
    // mutation: Mutation
})