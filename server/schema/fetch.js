const fs = require('fs')
const fetch = require('isomorphic-fetch');
const cheerio = require('cheerio');
const path = require("path");

async function fetchText() {
    const response = await fetch('https://news.ycombinator.com/news')
    const text = await response.text();
    const $ = cheerio.load(text);

    let allHrefArr = []
    let allTextArr = []
    $('a[class="storylink"]').each((index, element) => {
        allHrefArr.push($(element).attr('href'))
        allTextArr.push($(element).text())
    })

    let allPointsArr = []
    let allUserArr = []
    let allCommentArr = []
    $('td[class="subtext"]').each((index, element) => {
        if (element.firstChild.next.attribs.class === 'age') {
            allPointsArr.push('0 point')
            allUserArr.push('')
            allCommentArr.push('')
        } else if (element.firstChild.next.attribs.class === 'score') {
            allUserArr.push(element.firstChild.nextSibling.nextSibling.next.children[0].data)
            allPointsArr.push(element.firstChild.nextSibling.children[0].data)
            let numOfComment = element.lastChild.previousSibling.children[0].data
            if (numOfComment === "discuss") {
                allCommentArr.push('0 comment')
            } else {
                allCommentArr.push(numOfComment)
            }
        }
    })

    let allTimeArr = []
    $('span[class="age"]').each((index, element) => {
        allTimeArr.push($(element).text())
    })

    let database = []
    for (let i = 0; i < allTextArr.length; i++) {
        database[i] = {
            id: (i + 1).toString(),
            title: allTextArr[i],
            link: allHrefArr[i],
            author: allUserArr[i],
            commentCount: allCommentArr[i],
            points: allPointsArr[i],
            postTime: allTimeArr[i]
        }
    }

    fs.writeFileSync(path.join(__dirname + '/database.json'), JSON.stringify(database))

    return database
};

module.exports = { fetchText }
