Instructions:

Clone project: 
```
git clone git@gitlab.com:shinji1129/hackernewsscrape.git
```
---
\
Install packages & start project:

***Server side***
```
cd hackernewsscrape/server
yarn install
node app.js
```
(Should see "Listening on port 4000" in console) 

***React app***
```
cd hackernewsscrape/my-app
yarn install
yarn start
```
(Browser should be auto-launched with localhost:3000) 

---
\
Fetched data is stored in server/schema/database.json \
and is auto-fetched every 59sec:
```
[
    Posts:
  {
    id: GraphQLID,
    title: GraphQLString,
    author: GraphQLString,
    commentCount: GraphQLInt,
    points: GraphQLInt,
    postTime: GraphQLString,
    link: GraphQLString
  }, ...
]
```
---
\
Packages used:

***React app***
```
React, Typescript, Apollo-boost, React-Apollo, GraphQL, Material-UI (Card, CardContent, Typography, Link)
```

***Server***

```
Express, Graphql, Express-GraphQL, Cheerio, Isomorphic-Fetch, Cors, lodash
```
---
\
Known issues & future improvements:

- Should have coded backend with Typescript instead
- Didn't have enough time for bonus part
- Should learn more about GraphQL as it is a very convenient API for database
- Material-UI Table has built-in sorting and filtering function which could solve the first item in "Bonus Part". Seems to be a very convenient tool and will take a look into it.
- Stuck at deploying my app at Netlify for 2 whole days as it does not support backend, which totally conflicted my file system/structures... (I even bought a domain...) Will take a deep look into it when available.
