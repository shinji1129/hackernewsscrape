import React, { useEffect, useState } from 'react'
import './EachPost.css';
import { makeStyles } from "@material-ui/core/styles";
import Card from "@material-ui/core/Card";
import CardContent from "@material-ui/core/CardContent";
import Typography from "@material-ui/core/Typography";
import { gql } from 'apollo-boost'
import { useQuery } from 'react-apollo'
import { Link } from '@material-ui/core';

interface Post {
    id: number,
    title: string,
    author: string,
    commentCount: string,
    points: string,
    postTime: string,
    link: string
}

const useStyles = makeStyles({
    postContainer: {
        width: '100%',
        padding: '10px 15px 10px 15px',
        borderWidth: '1px',
        borderColor: '#ddd',
        borderRadius: '6px',
        borderStyle: 'solid',
        boxShadow: '5px 5px 13px 0px #555,  inset 0 0 13px #ccc;',
        marginBottom: '20px',
    },
    bullet: {
        display: "inline-block",
        margin: "0 2px",
        transform: "scale(0.8)"
    },
    title: {
        fontFamily: 'Syncopate',
        fontSize: '16px'
    },
    subText: {
        fontFamily: 'Varela Round',
    }
});

const GET_POSTS = gql`
query GET_POSTS{
  posts{
    id
    title
    author
    commentCount
    points
    postTime
    link
  }
}
`

const width = document.body.clientWidth
console.log(width)

const EachPost: React.FC<{}> = () => {

    const classes = useStyles();
    const [sortedList, setSortedList] = useState([]);
    const [isFocus, setIsFocus] = useState(false)
    const { loading, error, data } = useQuery(GET_POSTS)

    function scrollToBottom() {
        window.scroll({
            top: document.body.offsetHeight,
            behavior: 'smooth',
        });
    }
    function scrollToTop() {
        window.scroll({
            top: 0,
            behavior: 'smooth',
        });
    }

    useEffect(() => {
        if (data) {
            setSortedList(data.posts)
        }
    }, [data])

    if (loading) { return <div>Loading...</div> }
    if (error) { return <div style={{ fontFamily: 'Syncopate' }}>Error...</div> }

    console.log(data.posts)

    const sortByPostTime = () => {
        scrollToTop()
        const tempSortedList = sortedList.slice()
        tempSortedList.sort(function (a: Post, b: Post) {

            if (a.postTime.includes('hour') && b.postTime.includes('hour') || !a.postTime.includes('hour') && !b.postTime.includes('hour')) {
                let aPostTime = parseInt(a.postTime)
                let bPostTime = parseInt(b.postTime)
                if (aPostTime > bPostTime) return 1
                if (aPostTime < bPostTime) return -1
                return 0
            }
            if (a.postTime.includes('hour') && !b.postTime.includes('hour')) {
                let aPostTime = parseInt(a.postTime) * 60
                if (aPostTime > parseInt(b.postTime)) return 1
                if (aPostTime < parseInt(b.postTime)) return -1
                return 0
            }
            if (b.postTime.includes('hour') && !a.postTime.includes('hour')) {
                let bPostTime = parseInt(b.postTime) * 60
                if (parseInt(a.postTime) > bPostTime) return 1
                if (parseInt(a.postTime) < bPostTime) return -1
                return 0
            }
            return 0

        })
        setSortedList(tempSortedList)
    }
    const sortByCommentCount = () => {
        scrollToTop()
        const tempSortedList = sortedList.slice()
        tempSortedList.sort(function (a: Post, b: Post) {
            if (parseInt(a.commentCount) > parseInt(b.commentCount)) return -1
            if (parseInt(a.commentCount) < parseInt(b.commentCount)) return 1
            return 0
        })
        setSortedList(tempSortedList)
    }
    const sortByPoints = () => {
        scrollToTop()
        const tempSortedList = sortedList.slice()
        tempSortedList.sort(function (a: Post, b: Post) {
            if (parseInt(a.points) > parseInt(b.points)) return -1
            if (parseInt(a.points) < parseInt(b.points)) return 1
            return 0
        })
        setSortedList(tempSortedList)
    }

    return (
        <>
            <div>
                <div className={isFocus ? 'invisibleBgFocused' : 'invisibleBg'} onClick={() => { setIsFocus(!isFocus) }} />
                <div className="buttonList">
                    <div className={isFocus ? 'sortButtonBgFocused' : 'sortButtonBg'} >
                        <div className={isFocus ? 'sortFocused' : 'sort'} onClick={
                            () => { setIsFocus(!isFocus) }}>
                            SORT</div>
                        <div className={isFocus ? 'button1Focused' : "button"} onClick={() => {
                            sortByPostTime()
                        }}>Latest</div>
                        <div className={isFocus ? 'button2Focused' : "button"} onClick={() => {
                            sortByCommentCount()
                        }}>Most commented</div>
                        <div className={isFocus ? 'button3Focused' : "button"} onClick={() => {
                            sortByPoints()
                        }}>Most points</div>
                        <div className={isFocus ? 'button4Focused' : "button"} onClick={() => {
                            scrollToBottom()
                        }}>Scroll to bottom</div>

                    </div>
                </div>
                <div className='container'>
                    <div className="allPost">
                        {
                            sortedList.length > 0 &&
                            sortedList.map((post: { id: number; title: React.ReactNode; points: String; author: String; link: any; commentCount: String; postTime: String }) => (
                                loading ? (
                                    <p>Loading...</p>
                                ) : (
                                        <Card key={post.id}
                                            className={classes.postContainer} variant="outlined">
                                            <CardContent>
                                                <Typography
                                                    className={classes.title}
                                                    color="textSecondary"
                                                    component={'span'}
                                                    gutterBottom
                                                >
                                                    <Link href={post.link}>
                                                        {post.title}
                                                    </Link>
                                                </Typography>
                                                <Typography component={'span'} variant="body2" className={classes.subText}>
                                                    <div className='middleRow'>
                                                        <div>Posted by {post.author} ~ {post.postTime}</div>
                                                    </div>
                                                    <div className='bottomRow'>
                                                        <div>{post.points}</div>
                                                        <div>{post.commentCount}</div>
                                                    </div>
                                                </Typography>
                                            </CardContent>
                                        </Card>
                                    )
                            ))
                        }
                    </div>
                </div>
            </div>
        </>
    )
}

export default EachPost;
