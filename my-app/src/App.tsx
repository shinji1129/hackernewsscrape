import React from 'react';
import ApolloClient from 'apollo-boost'
import { ApolloProvider } from 'react-apollo'
import EachPost from './component/EachPost'

const client = new ApolloClient({
  uri: 'http://localhost:4000/graphql'
})

function App() {

  return (
    <div>
      <ApolloProvider client={client}>
        <EachPost />
      </ApolloProvider>
    </div >
  );
}

export default App;